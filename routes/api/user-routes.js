const router = require('express').Router();
const UserController = require('../../rest-controller/UserController');

router.get('/sendEmailToUser', UserController.sendEmailToUser);

router.get('/confirmUser', UserController.confirmUserAndUpdateDetails);

module.exports = router;