const router = require('express').Router();

router.use('/user', require('./api/user-routes'));

module.exports = router;