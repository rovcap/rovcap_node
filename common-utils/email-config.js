const sendGridMail = require("@sendgrid/mail");
const { EMAIL_FROM_USER_NAME, SEND_GRID_API_KEY } = require("../config/config");

function sendEmail(mailData) {
    sendGridMail.setApiKey(SEND_GRID_API_KEY);
    var transport = {
        to: mailData.to,
        from: EMAIL_FROM_USER_NAME,
        subject: mailData.subject,
        text: mailData.text,
        html: mailData.htmlTemplate
    };
    sendGridMail.send(transport).then(response => {console.log("Email Send successfully =====> ", response)}).
        catch(error => {console.log("Error in sending E-mail ===> ", error)});
}


module.exports = sendEmail;