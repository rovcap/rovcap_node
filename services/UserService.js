const Users = require('../models/user/Users');
const { Op } = require("sequelize");
const sendEmail = require("../common-utils/email-config")

class UserService {

    static async sendUserEmailForConfirmation(req) {
        var userId = req.query.userId;
        console.log("userId : ", userId);
        if (userId && userId != null && userId != undefined && userId != '') {
            var userDetails = await Users.findByPk(userId).then(data => userDetails = data);
            if (userDetails) {
                this.prepareDataToSendMail(userDetails);
                return true;
            } else {
                throw new Error("User Not found");
            }
        } else {
            throw new Error("Invalid User Id");
        }
    }

    static async prepareDataToSendMail(userDetails) {
        var url = "http://localhost:3000/api/v1/user/confirmUser?userId=" + userDetails.id;
        var mailData = {
            to: userDetails.email,
            subject: "User Email Confirmation",
            text: "User Email Confirmation",
            htmlTemplate: '<div style="text-align:center;"><a name="@mail/verify" href="' + url + '"'
                + '">Confirm Email</a></div>'
        };
        sendEmail(mailData);
    }

    static async confirmUserAndUpdateDetails(req) {
        console.log("req params inside confirmUserAndUpdateDetails : ", req.query);
        var userId = req.query.userId;
        console.log("userId : ", userId);
        if (userId && userId != null && userId != undefined && userId != '') {
            var userDetails = await Users.findByPk(userId).then(data => userDetails = data);
            if (userDetails) {
                userDetails = await Users.update({ confirmDateTime: new Date() }, { where: { id: userId } }).then(numberOfRowsAffected => userDetails = numberOfRowsAffected).catch(err => { console.log('err : ', err) });
                return userDetails;
            } else {
                throw new Error("User Not found");
            }
        } else {
            throw new Error("Invalid User Id");
        }
    }

    static async updateUserVisitsRecords(req) {
        console.log("req params inside updateUserVisitsRecords : ", req.query);
    }
}

module.exports = UserService;