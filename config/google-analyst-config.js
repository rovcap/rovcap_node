const { google } = require('googleapis');
const scopes = 'https://www.googleapis.com/auth/analytics.readonly';
const { GOOGLE_ANALYTICS_API_CLIENT_EMAIL, GOOGLE_ANALYTICS_API_PRIVATE_KEY } = require('./config');
const jwt = new google.auth.JWT(GOOGLE_ANALYTICS_API_CLIENT_EMAIL, null, GOOGLE_ANALYTICS_API_PRIVATE_KEY, scopes);

module.exports = {
    async getData() {
        const response = await jwt.authorize()
        const result = await google.analytics('v3').data.ga.get({
            'auth': jwt,
            'start-date': '30daysAgo',
            'end-date': 'today',
        })

        console.dir(result)
    }
}