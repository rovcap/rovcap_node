// config.js
const dotenv = require('dotenv');
dotenv.config();
module.exports = {
  ENV: process.env.ENV,
  PORT: process.env.PORT,
  DB_PORT: process.env.DB_PORT,
  HOST: process.env.HOST,
  USER: process.env.USER,
  PASSWORD: process.env.PASSWORD,
  SEND_GRID_API_KEY: process.env.SEND_GRID_API_KEY,
  EMAIL_FROM_USER_NAME: process.env.EMAIL_FROM_USER_NAME,
  EMAIL_FROM_USER_PASSWORD: process.env.EMAIL_FROM_USER_PASSWORD,  
  MIX_PANEL_USER_TRACKER_PROJECT_TOKEN: process.env.MIX_PANEL_USER_TRACKER_PROJECT_TOKEN,
  GOOGLE_ANALYTICS_API_CLIENT_EMAIL: process.env.GOOGLE_ANALYTICS_API_CLIENT_EMAIL,
  GOOGLE_ANALYTICS_API_PRIVATE_KEY: process.env.GOOGLE_ANALYTICS_API_PRIVATE_KEY
};