const Mixpanel = require('mixpanel');
const {MIX_PANEL_USER_TRACKER_PROJECT_TOKEN} = require('./config');
// create an instance of the mixpanel client
const mixpanel = Mixpanel.init(MIX_PANEL_USER_TRACKER_PROJECT_TOKEN);

module.exports = {
    trackUser: () => {
        mixpanel.track('Inside My Local-App');
        return true;
    }
}