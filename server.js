const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const PORT = 3000;
const Mixpanel = require('mixpanel');
const {MIX_PANEL_USER_TRACKER_PROJECT_TOKEN} = require('./config/config');
// create an instance of the mixpanel client
const mixpanel = Mixpanel.init(MIX_PANEL_USER_TRACKER_PROJECT_TOKEN);

app.use(cors());
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: true}));
app.use('/api/v1', require('./routes/index'));
// make public to the storage folder
app.use('/storage', express.static('storage'));

const sequelize = require('./config/sequelize-db');
  
// console.log(`Your port is ${process.env.PORT}`); // undefined
const dotenv = require('dotenv');
dotenv.config();
// console.log(`Your port is ${process.env.PORT}`);
app.listen(PORT, function() {
    console.log("Server is running on Port: " + PORT);
});
sequelize.sync().then(result => {
    // console.log('grfdfrgthgfdsfgfd ============> ', result);
}).catch(err => {
    console.log(err);
});