const UserService = require('../services/UserService');
const RestServiceTemplateUtils = require('../common-utils/RestServiceTemplateUtils');
const {trackUser} = require('../config/mixpanel-config');
const {getData} = require('../config/google-analyst-config');
class UserController {

    static async sendEmailToUser(req, res) {
        trackUser();
        getData();
        UserService.sendUserEmailForConfirmation(req).then(response => {
            RestServiceTemplateUtils.getRecordSuccessResponse(response, res);
        }).catch(error => {
            const err = { "error": error }
            RestServiceTemplateUtils.getRecordSuccessResponse(err, res);
        });
    }

    static async confirmUserAndUpdateDetails(req, res) {
        UserService.confirmUserAndUpdateDetails(req).then(response => {
            RestServiceTemplateUtils.getRecordSuccessResponse(response, res);
        }).catch(error => {
            const err = { "error": error }
            RestServiceTemplateUtils.getRecordSuccessResponse(err, res);
        });
    }

    static async updateUserVisitsRecords(req, res) {
        UserService.updateUserVisitsRecords(req).then(response => {
            RestServiceTemplateUtils.getRecordSuccessResponse(response, res);
        }).catch(error => {
            const err = { "error": error }
            RestServiceTemplateUtils.getRecordSuccessResponse(err, res);
        });
    }
}

module.exports = UserController;